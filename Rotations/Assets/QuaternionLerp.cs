﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuaternionLerp : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Position integration
        //transform.position += velocity * Time.deltaTime;

        // Position interpolation (linear interpolation: lerping)
        //transform.position = Vector3.Lerp(Start, End, t);

        if(Input.GetKeyDown(KeyCode.A))
            StartCoroutine(DoRotate(Quaternion.Euler(90, 0, 0), Quaternion.Euler(0, 180, 0)));

        if(Input.GetKeyDown(KeyCode.D))
            StartCoroutine(DoRotate(Quaternion.Euler(0, 180, 0), Quaternion.Euler(90, 0, 0)));
    }

    IEnumerator DoRotate(Quaternion a, Quaternion b)
    {
        float t = 0f;

        while(t <= 1f)
        {
            transform.rotation = Quaternion.Slerp(a, b, t);

            t += Time.deltaTime;
            yield return null;
        }
    }
    
    IEnumerator DoRotate(float startingAngle, float endingAngle)
    {
        float t = 0f;

        while(t <= 1f)
        {
            transform.rotation = Quaternion.Euler(Mathf.Lerp(startingAngle, endingAngle, t), Mathf.Lerp(startingAngle, endingAngle, t), 0);

            t += Time.deltaTime;
            yield return null;
        }
    }
}
