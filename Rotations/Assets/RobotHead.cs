﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotHead : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Mathematically, we apply these A*B*C*p
        // Position vector, p
        // C*p => p'
        // B*p' => p''
        // A*p'' => p''' (our answer)

        // Unity uses Tait-Bryan angles convention of ZXY order
        //transform.rotation = Quaternion.Euler(0, 90, 0) * Quaternion.Euler(90, 0, 0) * Quaternion.Euler(0, 0, 90);
        //transform.rotation = Quaternion.Euler(90, 90, 90);

        // WRONGEST!  NEVER EVER touch x, y, z, or w.
        //Quaternion q = new Quaternion(90, 90, 90, 0);

        Quaternion q = Quaternion.Euler(90, 90, 90);

        transform.position = q * new Vector3(20, 2, 0);

        q = Quaternion.AngleAxis(30, new Vector3(1, 0, 1));

        Vector3 enemyPos = new Vector3(30, 20, 10);

        q = Quaternion.LookRotation((enemyPos - transform.position).normalized, Vector3.up);
    }
}
