﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public int ammo = 3;
    [SerializeField] Vector3 ammoUIPosition = Vector3.zero;
    [SerializeField] GameObject ammoUIPrefab;


    [SerializeField] GameObject turret;

    private float turretAngle = 0f;


    private LineRenderer laserLine;



    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < ammo; ++i)
        {
            // TODO: Add to a list to enable/disable according to how much ammo the player uses
            Instantiate(ammoUIPrefab, ammoUIPosition + new Vector3(12.8f + 0.6f*i, 12.36f, -9.77f), ammoUIPrefab.transform.rotation);
        }

        laserLine = GetComponent<LineRenderer>();
    }

    IEnumerator SomeCoroutine()
    {
        yield return new WaitForSeconds(5);  // Affected by timeScale
        yield return new WaitForSecondsRealtime(5);  // Unaffected by timeScale
        //Time.unscaledDeltaTime
        //Time.unscaledTime
        //Time.realtimeSinceStartup
    }

    // Update is called once per frame
    void Update()
    {
        /*if (PauseMenu.Instance.IsPaused())
            return;*/

        //int layerIndex = LayerMask.NameToLayer("MyLayer");


        float turretHoriz = Input.GetAxis("Horizontal");  // -1 to 1
        float turretVert = Input.GetAxis("Vertical"); // -1 to 1

        // Left: -1 horiz, 0 vert

        // sohcahtoa
        // sin(θ) = opp/hyp
        // cos(θ) = adj/hyp
        // tan(θ) = opp/adj

        // Velocity: 15 m/s at 30 degrees from the +x axis
        //     Knowing the direction and magnitude, we can get the components (x and y separately)
        // hyp = 15
        // θ = 30 degrees

        // adj = hyp*cos(θ)
        // x = mag * cos(θ)

        // opp = hyp*sin(θ)
        // y = mag * sin(θ)


        // Vector: (x, y) -> triangle with legs of length x and y
        // Velocity: 20 m/s to the right (+x), 5 m/s up (+y)
        //     Knowing the legs of the triangle, we can get the angle

        // tan(θ) = opp/adj
        // tan(θ) = y / x
        // inverse tangent: tan^-1(tan(θ)) = θ
        // (aka arctangent): atan()
        // θ = atan(y/x)

        // 90 degrees = atan(5 / 0)
        // 0 degrees = atan(0 / 5)
        // 180 degrees = atan(0 / -5)

        // θ = atan2(y, x)


        // Magnitude?  Hypotenuse?
        // Pythagorean theorem (or, equivalently, the distance formula)
        // a^2 + b^2 = c^2
        // c = sqrt(a^2 + b^2) = sqrt(a*a + b*b)


        // Angular measure in code typically uses RADIANS
        // 180 degrees = pi radians
        // Degrees -> Radians: pi/180 * myAngleDegrees = myAngleRadians
        // Radians -> Degrees: 180/pi * myAngleRadians = myAngleDegrees

        //Mathf.Deg2Rad()
        //Mathf.Rad2Deg()



        /*Mathf.Cos();
        Mathf.Sin();
        Mathf.Tan();*/

        //Mathf.SmoothDampAngle()

        // What if turretHoriz is 0.0000000001f?
        if (Mathf.Abs(turretHoriz) > 0.1f || Mathf.Abs(turretVert) > 0.1f)
        {
            float newTurretAngle = (180f / Mathf.PI) * Mathf.Atan2(turretHoriz, turretVert);

            // The NAUGHTY way...  Using Lerp() to perform NON-LINEAR interpolation
            turretAngle = Mathf.Lerp(turretAngle, newTurretAngle, 0.1f);
            turret.transform.rotation = Quaternion.Euler(0f, turretAngle, 0f);

            // This is cheating.  Less efficient and make sure you know how to solve these problems without Unity holding your hand.
            //turret.transform.rotation = Quaternion.LookRotation(new Vector3(turretHoriz, 0f, turretVert));
        }

        RaycastHit hit;
        if (Physics.Raycast(turret.transform.position, turret.transform.forward, out hit))
        {
            //hit.point
            //hit.collider.CompareTag("Player")
            //hit.distance
            laserLine.enabled = true;
            laserLine.positionCount = 2;
            laserLine.SetPosition(0, turret.transform.position);
            laserLine.SetPosition(1, hit.point);
        }
        else
            laserLine.enabled = false;


        /*
         
        Bullet -> Enemy


        Bullet:
        Position
        Facing (rotation)
        Velocity

        Enemy:
        Position
        Facing


        Vector3 separation = bullet.transform.position - enemy.transform.position;

        // If the bullet is behind the enemy, it will hurt
        float alignment = Vector3.Dot(enemy.transform.forward, separation.normalized);

        BULLET
                Separation
                   V
          BULLET <----- ENEMY --->          BULLET
                               ^
                            Forward

        // Dot product: Vector multiplication that gives us a scalar (normal numbers) result
        // alignment will be between -1 and +1
        // -1 means anti-aligned (opposite directions)
        // 0 means perpendicular
        // 1 means perfectly aligned (same direction)
        if(alignment < -0.7f)
            GetHurt();
         */
    }
}
