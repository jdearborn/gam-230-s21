﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cactus : MonoBehaviour
{
    static public int cactusCount = 0;

    private void Awake()
    {
        //cactusCount++;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        //cactusCount--;
    }

    private void OnMouseDown()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    private void OnEnable()
    {
        cactusCount++;
    }

    private void OnDisable()
    {
        cactusCount--;
    }
}
