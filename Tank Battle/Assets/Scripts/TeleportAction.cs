﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportAction : MonoBehaviour
{
    Material mat;

    // Start is called before the first frame update
    void Start()
    {
        Renderer rend = GetComponentInChildren<Renderer>();
        mat = rend.material;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(DoTeleport(transform.forward, 5, 3f));
        }
    }

    IEnumerator DoTeleport(Vector3 direction, float distance, float rate)
    {
        // Make dude disappear (adjust AlphaThreshold)
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime * rate;
            mat.SetFloat("AlphaThreshold", t);  // go from 0 to 1
            yield return null;
        }

        // Move dude to destination
        transform.position += direction * distance;

        // Make dude reappear
        while (t > 0f)
        {
            t -= Time.deltaTime * rate;
            mat.SetFloat("AlphaThreshold", t);  // go from 1 to 0
            yield return null;
        }
    }

}
