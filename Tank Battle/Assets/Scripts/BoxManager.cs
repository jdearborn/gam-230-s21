﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour
{
    private List<GameObject> boxes = new List<GameObject>();
    [SerializeField] GameObject boxPrefab;

    public static BoxManager Instance { get { return _instance; } }
    private static BoxManager _instance;

    [SerializeField] private PauseMenu pauseMenu;

    private void Awake()
    {
        // Halfway to the Singleton pattern
        // (if there are more than one BoxManager, then the last one is the active one)
        _instance = this;
        // Warning: Scene transitions will technically make the _instance invalid.
    }

    // Start is called before the first frame update
    void Start()
    {
        SpawnBox(new Vector3(5, 0, 3));
        SpawnBox(new Vector3(4, 0, 7));
        SpawnBox(new Vector3(2, 0, -4));
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Cactuses: " + Cactus.cactusCount);

            Debug.Log("List of cactuses: " + boxes.Count);
        }


        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(pauseMenu.IsPaused())
                pauseMenu.Unpause();
            else
                pauseMenu.Pause();
        }



    }

    public void SpawnBox(Vector3 position)
    {
        GameObject go = Instantiate(boxPrefab, position, Quaternion.identity);
        boxes.Add(go);
    }

    public void RemoveBox(int index)
    {
        if (index < 0 || index >= boxes.Count)
            return;

        Destroy(boxes[index]);
        boxes.RemoveAt(index);
    }

    public int GetNumBoxes()
    {
        return boxes.Count;
    }
}
