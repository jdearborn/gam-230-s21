﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private bool isPaused = false;

    public bool IsPaused()
    {
        return isPaused;
    }

    public void Pause()
    {
        isPaused = true;
        Time.timeScale = 0f;

        gameObject.SetActive(true);
    }

    public void Unpause()
    {
        isPaused = false;
        Time.timeScale = 1f;

        gameObject.SetActive(false);
    }
}
