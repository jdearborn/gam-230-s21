﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedModAdjuster : MonoBehaviour
{
    private Renderer rend;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time < 5)
            rend.material.SetFloat("RedMod", 1f);
        else
            rend.material.SetFloat("RedMod", 0f);
    }
}
