﻿Shader "ImageEffects/RedShift"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        RedOffset("Distance to shift", Range(-0.05, 0.05)) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float RedOffset;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                float4 offsetSample = tex2D(_MainTex, i.uv + float2(RedOffset, 0));
                col = float4(offsetSample.r, col.gba);
                
                return col;
            }
            ENDCG
        }
    }
}
