﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedShiftEffect : MonoBehaviour
{
    [SerializeField] private Material mat;


    private void Update()
    {
        if(Input.GetButtonDown("Jump"))
            StartCoroutine(DoShift(10f));
    }

    IEnumerator DoShift(float speed)
    {
        for (float t = 0f; t <= 1f; t += speed * Time.deltaTime)
        {
            mat.SetFloat("RedOffset", t * 0.01f);
            yield return null;
        }

        for (float t = 0f; t <= 1f; t += speed * Time.deltaTime)
        {
            mat.SetFloat("RedOffset", (1-t) * 0.01f);
            yield return null;
        }

    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, mat);
    }
}
