﻿Shader "Unlit/CrazyTankShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        RedMod ("Red modulation", Range(0, 1)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float RedMod;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);

                o.normal = v.normal;
                return o;
            }

            float4 frag(v2f i) : SV_Target
            {
                float t = _Time[1];
                // sample the texture
                float4 col = tex2D(_MainTex, i.uv);
                //float4 col = 1 - float4(0, 1, 0, 1);

                // sin() -> -1 to +1
                // r -> 0 to 1
                // sin()+1 -> 0 to 2
                // (sin()+1)/2 -> 0 to 1
                //float r = ((sin(t) + 1) / 2 + 0.5) / 1.5;


                // Vector swizzling
                //col = float4(r, r, r, 1) * col;
                //col = float4(col.b, col.r, col.g, col.a);
                //col = float4(col.rg, 0.5, col.a);
                //col = col.bbba;


                //col = float4(i.uv.x, i.uv.y, 1, 1);
                //col = float4(i.normal.x, i.normal.y, i.normal.z, 1);

                col = float4(RedMod*col.r, col.gba);


                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
