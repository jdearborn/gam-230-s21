﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Want: Access to score at all times
    // Want: Score to not be destroyed when scene changes
    // Want: One place to store the score.

    // These are satisfied by a Singleton
    // Singleton pattern: Only ever ONE and has global access

    private static GameManager _instance = null;
    public static GameManager Instance { get { return _instance; } }


    public int score = 0;

    // Called immediately when this object is created, before any Start() is called
    void Awake()
    {
        if (_instance == null)
        {
            // This is the first GameManager object!
            _instance = this;

            // Don't let this object get destroyed by scene transitions
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            // This is not the one true GameManager.  Destroy it!
            Destroy(gameObject);
        }
    }
}
