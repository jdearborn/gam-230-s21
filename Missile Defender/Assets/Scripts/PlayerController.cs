﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject otherObject;

    [SerializeField]
    private int secrets = 4;

    // public, protected, private access specifiers
    protected int tempHitpoints = 50;  // protected: child/derived classes (through inheritance) can read or modify this variable
    private int actualHitpoints = 50;  // no code outside of this class can read or modify this variable

    [SerializeField] private GameObject projectilePrefab;

    public int GetHitpoints()  // accessor
    {
        return tempHitpoints;
    }

    public void SetHitpoints(int newHitpoints)  // mutator
    {
        tempHitpoints = newHitpoints;
        actualHitpoints = newHitpoints;
    }

    // C# property
    //public int hitpoints { get; set; }
    public int hitpoints { get { return tempHitpoints; } set { tempHitpoints = value; } }

    // static: Only one of these variables, shared across all instances of this class
    static public int playerCounter = 0;




    // Start is called before the first frame update
    void Start()
    {
        playerCounter++;

        hitpoints = 5;
    }

    // Update is called once per frame
    void Update()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        float speed = 10.0f;

        // Vector possibilities: <1, 0, 1>
        //                       <1, 0, 0>
        //                       <-1, 0, 1>

        Vector3 myVector = new Vector3(horiz, 0f, vert);

        // Pythagorean theorem or Distance formula
        // a^2 + b^2 = c^2
        // mag = sqrt(x^2 + y^2 + z^2);
        // Don't use carat (^)!  This is the bitwise XOR operator, not exponentiation.
        // 11001 | 01011 == 11011
        // 11001 & 01011 == 01001
        // 11001 ^ 01011 == 10010

        float mag = Mathf.Sqrt(myVector.x*myVector.x + myVector.y*myVector.y + myVector.z*myVector.z);
        myVector = myVector / mag;

        //Debug.Log("myVector: " + myVector.magnitude);


        transform.position += speed * new Vector3(horiz, 0f, vert).normalized * Time.deltaTime;



        // if(Input.GetButton("Fire1"))  // Current state of the button (every frame)
        if(Input.GetButtonDown("Fire1"))  // Only true on the frame it happened
        {
            //Instantiate(projectilePrefab, transform.position, Quaternion.identity);
            GameObject go = Instantiate(projectilePrefab, transform.position, transform.rotation);

            Rigidbody rb = go.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.velocity = 40f * transform.forward;
            }
        }
        //Input.GetButtonUp  // Only true on the frame the button was released
        /*if(Input.GetKeyDown(KeyCode.S) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
        {
            // Save game?  ctrl-s
        }*/

        //Input.mousePosition
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
        }
    }
}
