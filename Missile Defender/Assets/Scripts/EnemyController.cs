﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private GameObject playerObject;

    public float speed = 1.0f;

    [SerializeField] private ParticleSystem deathParticles;

    private static int enemyCounter = 0;
    public static int GetEnemyCount() // Accessor function
    {
        return enemyCounter;
    }
    public static int EnemyCount { get { return enemyCounter; } }  // C# property (redundant)

    private void Awake()
    {
        enemyCounter++;
    }

    // Start is called before the first frame update
    void Start()
    {
        playerObject = MissileDefenderLevel.Instance.player.gameObject;
    }

    private void OnDestroy()
    {
        enemyCounter--;
    }

    // Update is called once per frame
    void Update()
    {
        // d = <1, 4, 0> + <3, 2, -6> = <4, 6, -6>
        // d = <1, 4, 0> - <3, 2, -6> = <-2, 2, 6>
        Vector3 direction = (playerObject.transform.position - transform.position).normalized;

        transform.position += speed * direction * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("PlayerProjectile"))
        {
            ParticleSystem ps = Instantiate(deathParticles, transform.position, Quaternion.identity);

            // Kill the particle system object after a certain number of seconds
            Destroy(ps.gameObject, ps.main.startLifetime.Evaluate(0f));

            //MissileDefenderLevel.Instance.AddScore(100);
            GameManager.Instance.score += 100;

            Destroy(gameObject);
        }
    }
}
