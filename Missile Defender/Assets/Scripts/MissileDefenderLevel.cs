﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;  // For normal UI features and objects
using TMPro;  // For TextMeshPro object
using UnityEngine.SceneManagement;

public class MissileDefenderLevel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timerValue;
    public float waveTimer = 10f;

    [SerializeField] private GameObject enemyPrefab;

    [SerializeField] private List<WaveDef> waves = new List<WaveDef>();

    public PlayerController player;

    // Singleton pattern
    private static MissileDefenderLevel _instance = null;
    public static MissileDefenderLevel Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }



    // Update is called once per frame
    void Update()
    {
        timerValue.text = string.Format("{0:F1}", waveTimer);

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            waves.Clear();
        }

        if(waveTimer > 0f)
        {
            // Timer is still ticking down...
            waveTimer -= Time.deltaTime;

            if(waveTimer <= 0f)
            {
                // Timer just ran out!
                waveTimer = 0f;

                // Spawn next wave
                if(waves.Count > 0)
                {
                    Debug.Log("Spawning new wave of " + waves[0].positions.Count + " enemies!");
                    waves[0].Spawn(enemyPrefab);
                    waves.RemoveAt(0);
                }
                else
                {
                    // Make sure all enemies are dead first...
                    if (EnemyController.GetEnemyCount() == 0)
                    {
                        // You win!
                        Debug.Log("You win!");
                        SceneManager.LoadScene("WinScreen");
                    }
                }

                // Restart timer
                waveTimer = 10f;
            }
        }
    }
}
