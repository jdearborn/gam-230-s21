﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinScreen : MonoBehaviour
{
    public void RestartGame(string newScene)
    {
        Debug.Log("Restarting game...");
        SceneManager.LoadScene(newScene);
    }
}
