﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveDef
{
    public List<Vector3> positions = new List<Vector3>();

    public void Spawn(GameObject prefab)
    {
        for(int i = 0; i < positions.Count; ++i)
        {
            GameObject.Instantiate(prefab, positions[i], Quaternion.identity);
        }
    }
}
