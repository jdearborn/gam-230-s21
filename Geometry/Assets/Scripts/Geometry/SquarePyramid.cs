﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class SquarePyramid : MonoBehaviour
{
    public GameObject vertexHelper;

    private Mesh mesh;

    private List<Vector3> faceNormals = new List<Vector3>();
    private List<Vector3> faceCenters = new List<Vector3>();

    private void CreateMesh()
    {
        mesh = new Mesh();

        // Quad lies in x-z plane, facing +y
        List<Vector3> vertices = new List<Vector3>();
        // Looking at the faces from the front (looking toward -y)
        vertices.Add(new Vector3(-1, 0, -1));  // 0 - lower right
        vertices.Add(new Vector3(1, 0, -1));  // 1 - lower left
        vertices.Add(new Vector3(1, 0, 1)); // 2 - upper left
        vertices.Add(new Vector3(-1, 0, 1)); // 3 - upper right

        vertices.Add(new Vector3(0, 2, 0)); // 4 - top point

        /*for(int i = 0; i < vertices.Count; ++i)
        {
            GameObject go = Instantiate(vertexHelper, vertices[i], Quaternion.identity);
            VertexHelper vh = go.GetComponent<VertexHelper>();
            vh.index = i;
        }*/



        // Clockwise is the front face winding


        List<int> indices = new List<int>();
        indices.Add(0);
        indices.Add(1);
        indices.Add(2);

        indices.Add(0);
        indices.Add(2);
        indices.Add(3);

        indices.Add(4);
        indices.Add(0);
        indices.Add(3);

        indices.Add(4);
        indices.Add(1);
        indices.Add(0);

        indices.Add(4);
        indices.Add(2);
        indices.Add(1);

        indices.Add(4);
        indices.Add(3);
        indices.Add(2);









        // Copy the vertices that each index refers to so we can get individual normals for each triangle.  Ugh...
        List<Vector3> actualVertices = new List<Vector3>();
        for(int i = 0; i < indices.Count; ++i)
        {
            actualVertices.Add(vertices[indices[i]]);
        }

        // Reset our indices to go in sequence through all of our new actual vertices.
        // NOTE: This ruins to performance gains from using indices in the first place!
        indices.Clear();
        for(int i = 0; i < actualVertices.Count; ++i)
        {
            indices.Add(i);
        }

        mesh.SetVertices(actualVertices);

        mesh.SetTriangles(indices, 0);


        // Set the UVs / texture coordinates
        List<Vector2> uvs = new List<Vector2>();


        // Square base mapping
        Vector2 uvA = new Vector2(0.8f, 1f - 0);  // UL
        Vector2 uvB = new Vector2(1f, 1f - 0);  // UR
        Vector2 uvC = new Vector2(1f, 1f - 0.25f);  // LR
        Vector2 uvD = new Vector2(0.8f, 1f - 0.25f);  // LL

        uvs.Add(uvC);
        uvs.Add(uvD);
        uvs.Add(uvA);

        uvs.Add(uvC);
        uvs.Add(uvA);
        uvs.Add(uvB);


        // Triangle sides mapping
        for (int n = 0; n < 4; ++n)
        {
            Vector2 uv0 = new Vector2(0f + n * 0.2f, 1f - 0.25f);  // left-lower texcoord
            Vector2 uv1 = new Vector2(0.2f + n * 0.2f, 1f - 0.25f);  // right-lower texcoord
            Vector2 uv2 = new Vector2(0.1f + n * 0.2f, 1f - 0.0f);  // center-top texcoord

            uvs.Add(uv0);
            uvs.Add(uv2);
            uvs.Add(uv1);
        }

        mesh.SetUVs(0, uvs);


        // Calculate our own normals
        List<Vector3> normals = new List<Vector3>();
        for(int i = 0; i < indices.Count; i += 3)
        {
            int n = i / 3;

            Vector3 a = actualVertices[i];
            Vector3 b = actualVertices[i+1];
            Vector3 c = actualVertices[i+2];

            // Two (common) ways to multiply vectors: Dot product and the Cross product
            // Dot product: Tells you how aligned two vectors are.
            // Cross product: Constructs a mutually perpendicular new vector
            Vector3 normal = Vector3.Cross(a - b, a - c).normalized;

            normals.Add(normal);
            normals.Add(normal);
            normals.Add(normal);
        }
        mesh.SetNormals(normals);
        //mesh.RecalculateNormals();


        // Let's go through each face (skipping the extra triangle for the square base)
        // Calculate the position of the center and the normal
        for(int n = 1; n < 6; ++n)
        {
            Vector3 a = actualVertices[n * 3];
            Vector3 b = actualVertices[n * 3 + 1];
            Vector3 c = actualVertices[n * 3 + 2];

            faceCenters.Add(0.95f * (a + b + c)/3f);

            Vector3 normal = Vector3.Cross(a - b, a - c).normalized;
            faceNormals.Add(normal);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        CreateMesh();

        MeshFilter mf = GetComponent<MeshFilter>();
        mf.mesh = mesh;

        MeshCollider mc = GetComponent<MeshCollider>();
        mc.sharedMesh = mesh;
    }

    private void Update()
    {
        // TODO?: Raycast using my faceCenters and faceNormals to detect where the ground is

        // Maybe do this oncollisionstay
        for(int f = 0; f < faceCenters.Count; ++f)
        {
            Vector3 faceDirection = transform.rotation * faceNormals[f];
            //new Vector3(0, 1, 0);
            float alignment = Vector3.Dot(Vector3.down, faceDirection);  // Using down for this weird die (no face points upward)
            // -1 means oppositely aligned, 0 means perpendicular, 1 means aligned in same direction
            if(alignment > 0.8f)
            {
                Debug.Log("Landed on " + f);
            }
        }
    }

    void OnDrawGizmos()
    {
        for (int f = 0; f < faceCenters.Count; ++f)
        {
            if (Physics.Raycast(transform.rotation * faceCenters[f] + transform.position, transform.rotation * faceNormals[f], 0.3f))
            {
                Gizmos.color = Color.red;
            }
            else
            {
                Gizmos.color = Color.white;
            }
            Gizmos.DrawRay(transform.rotation * faceCenters[f] + transform.position, transform.rotation * faceNormals[f]);
        }
    }
}
