﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triangle : MonoBehaviour
{
    private Mesh mesh;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();

        List<Vector3> vertices = new List<Vector3>();
        vertices.Add(new Vector3(0, 1, 0));  // 0
        vertices.Add(new Vector3(1, 0, 0));  // 1
        vertices.Add(new Vector3(-1, 0, 0)); // 2

        mesh.SetVertices(vertices);

        List<int> indices = new List<int>();
        indices.Add(0);
        indices.Add(1);
        indices.Add(2);

        mesh.SetTriangles(indices, 0);

        mesh.RecalculateNormals();


        MeshFilter mf = GetComponent<MeshFilter>();

        mf.mesh = mesh;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
