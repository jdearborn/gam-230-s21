﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] private Camera cam;

    private int airJumpsLeft = 1;
    private float isGroundedTimer = -1f;  // Greater than 0?  You're grounded!

    [SerializeField] private AudioClip jumpSound;

    private GameObject platformParent = null;
    private Vector3 platformParentOldPosition = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void ApplyJumpImpulse()
    {
        //rb.velocity.y = 0f; // No!!  It's a property, sorry.
        //Debug.Log(rb.velocity.y);

        //rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        AudioSource.PlayClipAtPoint(jumpSound, transform.position, 0.3f);

        // myAudioSource.PlayOneShot(mySound, 0.5f);

        Vector3 newVelocity = rb.velocity;
        newVelocity.y = 0f;
        rb.velocity = newVelocity;

        rb.AddForce(10f * Vector3.up, ForceMode.Impulse);  // DRY - Don't Repeat Yourself, indicates this should maybe be put in a "Jump" function
    }

    // Update is called once per frame
    void Update()
    {
        isGroundedTimer -= 10f*Time.deltaTime;

        if(Input.GetButtonDown("Jump"))
        {
            if (isGroundedTimer > 0f)
            {
                ApplyJumpImpulse();
                isGroundedTimer = -1;
            }
            else if(airJumpsLeft > 0)
            {
                ApplyJumpImpulse();
                airJumpsLeft--;
            }
        }

        if (platformParent != null)
        {
            transform.position += (platformParent.transform.position - platformParentOldPosition);
            platformParentOldPosition = platformParent.transform.position;
        }
    }

    // FixedUpdate is called once per physics engine "tick"/time step
    private void FixedUpdate()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horiz, 0f, vert).normalized;

        Vector3 force = 50f * direction;
        rb.AddForce(cam.transform.rotation * force);


        Vector3 facing = transform.forward;
        float t = 0.5f;  // Leaving this here (for now)...
        // Linear interpolation
        Vector3 interpolatedFacing = (1-t)*facing + t*direction;
        transform.rotation = Quaternion.LookRotation(interpolatedFacing);

    }

    private void OnTriggerStay(Collider other)
    {
        isGroundedTimer = 1f;
        airJumpsLeft = 1;

        if (platformParent == null)
        {
            platformParent = other.gameObject;
            platformParentOldPosition = platformParent.transform.position;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        platformParent = null;
        platformParentOldPosition = Vector3.zero;
    }

}
