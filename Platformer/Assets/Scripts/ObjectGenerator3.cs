﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator3 : MonoBehaviour
{
    [SerializeField] GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnObjectForever(prefab, 3f, 1f));
    }

    IEnumerator SpawnObjectForever(GameObject obj, float startDelay, float repeatDelay)
    {
        yield return new WaitForSeconds(startDelay);

        while (true)
        {
            Instantiate(obj, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(repeatDelay + Random.Range(-0.5f, 0.5f));
        }

    }
}
