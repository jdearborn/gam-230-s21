﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointMover : MonoBehaviour
{
    [SerializeField] private List<Transform> waypoints = new List<Transform>();
    private float maxWaypointTimer = 2f;
    private float nextWaypointTimer;

    private Vector3 lastPosition;
    private int nextIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        nextWaypointTimer = maxWaypointTimer;
        lastPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        nextWaypointTimer -= Time.deltaTime;
        if(nextWaypointTimer < 0f)
        {
            nextWaypointTimer = maxWaypointTimer;
            lastPosition = waypoints[nextIndex].position;

            // Now start moving to the next waypoint
            ++nextIndex;
            if (nextIndex >= waypoints.Count)
                nextIndex = 0;
        }

        float t = (maxWaypointTimer - nextWaypointTimer) / maxWaypointTimer;  // Modify timer to fit into 0-1 range
        transform.position = Vector3.Lerp(lastPosition, waypoints[nextIndex].position, t);
    }
}
