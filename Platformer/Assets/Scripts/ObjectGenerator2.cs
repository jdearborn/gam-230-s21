﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator2 : MonoBehaviour
{
    public float startDelay = 5f;
    public float repeatDelay = 3f;
    [SerializeField] private GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        //Invoke("SpawnObject", 3f);  // Call this function 3 seconds later
        InvokeRepeating("SpawnObject", startDelay, repeatDelay);

        //CancelInvoke();
    }

    void SpawnObject()
    {
        Instantiate(prefab, transform.position, Quaternion.identity);
    }
}
